-- Wireshark LUA dissector for the [Sony] Ericsson's DebugMux protocol
-- (C) 2024 by Vadim Yanitskiy <fixeria@osmocom.org>
-- SPDX-License-Identifier: GPL-2.0+
--
-- Usage: Move this file to your "personal lua plugins" folder that
-- can be found in the Wireshark Help->About Wireshark->Folders tab
-- Windows: %APPDATA%\Wireshark\plugins.
-- Unix-like systems: ~/.local/lib/wireshark/plugins.

sedbgmux_proto = Proto("SEDbgMux", "[Sony] Ericsson's DebugMux protocol")

sedbgmux_proto.prefs["check_fcs"] =
    Pref.bool("Validate FCS (Frame Check Sequence)", true,
              "Enable/disable FCS (Frame Check Sequence) validation")

local FCS16Lookup = {
    0x0000,0x1189,0x2312,0x329B,0x4624,0x57AD,0x6536,0x74BF,
    0x8C48,0x9DC1,0xAF5A,0xBED3,0xCA6C,0xDBE5,0xE97E,0xF8F7,
    0x1081,0x0108,0x3393,0x221A,0x56A5,0x472C,0x75B7,0x643E,
    0x9CC9,0x8D40,0xBFDB,0xAE52,0xDAED,0xCB64,0xF9FF,0xE876,
    0x2102,0x308B,0x0210,0x1399,0x6726,0x76AF,0x4434,0x55BD,
    0xAD4A,0xBCC3,0x8E58,0x9FD1,0xEB6E,0xFAE7,0xC87C,0xD9F5,
    0x3183,0x200A,0x1291,0x0318,0x77A7,0x662E,0x54B5,0x453C,
    0xBDCB,0xAC42,0x9ED9,0x8F50,0xFBEF,0xEA66,0xD8FD,0xC974,
    0x4204,0x538D,0x6116,0x709F,0x0420,0x15A9,0x2732,0x36BB,
    0xCE4C,0xDFC5,0xED5E,0xFCD7,0x8868,0x99E1,0xAB7A,0xBAF3,
    0x5285,0x430C,0x7197,0x601E,0x14A1,0x0528,0x37B3,0x263A,
    0xDECD,0xCF44,0xFDDF,0xEC56,0x98E9,0x8960,0xBBFB,0xAA72,
    0x6306,0x728F,0x4014,0x519D,0x2522,0x34AB,0x0630,0x17B9,
    0xEF4E,0xFEC7,0xCC5C,0xDDD5,0xA96A,0xB8E3,0x8A78,0x9BF1,
    0x7387,0x620E,0x5095,0x411C,0x35A3,0x242A,0x16B1,0x0738,
    0xFFCF,0xEE46,0xDCDD,0xCD54,0xB9EB,0xA862,0x9AF9,0x8B70,
    0x8408,0x9581,0xA71A,0xB693,0xC22C,0xD3A5,0xE13E,0xF0B7,
    0x0840,0x19C9,0x2B52,0x3ADB,0x4E64,0x5FED,0x6D76,0x7CFF,
    0x9489,0x8500,0xB79B,0xA612,0xD2AD,0xC324,0xF1BF,0xE036,
    0x18C1,0x0948,0x3BD3,0x2A5A,0x5EE5,0x4F6C,0x7DF7,0x6C7E,
    0xA50A,0xB483,0x8618,0x9791,0xE32E,0xF2A7,0xC03C,0xD1B5,
    0x2942,0x38CB,0x0A50,0x1BD9,0x6F66,0x7EEF,0x4C74,0x5DFD,
    0xB58B,0xA402,0x9699,0x8710,0xF3AF,0xE226,0xD0BD,0xC134,
    0x39C3,0x284A,0x1AD1,0x0B58,0x7FE7,0x6E6E,0x5CF5,0x4D7C,
    0xC60C,0xD785,0xE51E,0xF497,0x8028,0x91A1,0xA33A,0xB2B3,
    0x4A44,0x5BCD,0x6956,0x78DF,0x0C60,0x1DE9,0x2F72,0x3EFB,
    0xD68D,0xC704,0xF59F,0xE416,0x90A9,0x8120,0xB3BB,0xA232,
    0x5AC5,0x4B4C,0x79D7,0x685E,0x1CE1,0x0D68,0x3FF3,0x2E7A,
    0xE70E,0xF687,0xC41C,0xD595,0xA12A,0xB0A3,0x8238,0x93B1,
    0x6B46,0x7ACF,0x4854,0x59DD,0x2D62,0x3CEB,0x0E70,0x1FF9,
    0xF78F,0xE606,0xD49D,0xC514,0xB1AB,0xA022,0x92B9,0x8330,
    0x7BC7,0x6A4E,0x58D5,0x495C,0x3DE3,0x2C6A,0x1EF1,0x0F78,
}

function calc_fcs(buffer)
    local crc = 0xffff
    for i = 0, buffer:len() - 1 do
        local val = buffer(i, 1):uint()
        local idx = bit32.bxor(val, bit32.band(crc, 0xff)) + 1
        crc = bit32.bxor(FCS16Lookup[idx], bit32.rshift(crc, 8))
    end
    return bit32.bxor(crc, 0xffff)
end

local MsgType_vs = {
    [0x65] = "Enquiry",
    [0x66] = "Ident",
    [0x67] = "Ping",
    [0x68] = "Pong",
    [0x69] = "DPAnnounce",
--  [0x6a] = "UNKNOWN",
    [0x6b] = "ConnEstablish",
    [0x6c] = "ConnEstablished",
    [0x6d] = "ConnTerminate",
    [0x6e] = "ConnTerminated",
    [0x6f] = "ConnData",
    [0x70] = "FlowControl",
    [0x71] = "Ack",
}

local f = sedbgmux_proto.fields

-- DebugMux frame fields
f.Marker = ProtoField.bytes("sedbgmux.marker", "Frame Marker")
f.Length = ProtoField.uint16("sedbgmux.length", "Frame Length", base.DEC)
f.TxCount = ProtoField.uint8("sedbgmux.tx_count", "Tx Count", base.DEC)
f.RxCount = ProtoField.uint8("sedbgmux.rx_count", "Rx Count", base.DEC)
f.MsgType = ProtoField.uint8("sedbgmux.msg_type", "Message Type", base.HEX_DEC, MsgType_vs)
f.MsgData = ProtoField.bytes("sedbgmux.msg_data", "Message Data")
f.FCS = ProtoField.uint16("sedbgmux.fcs", "Frame Check Sequence")
-- Ident fields
f.IdentMagic = ProtoField.bytes("sedbgmux.ident_magic", "Magic")
f.IdentLength = ProtoField.uint8("sedbgmux.ident_length", "Identity Length")
f.IdentString = ProtoField.string("sedbgmux.ident_string", "Identity String")
-- Ping/Pong fields
f.PingPongLength = ProtoField.uint8("sedbgmux.ping_length", "Payload Length")
f.PingPongString = ProtoField.string("sedbgmux.ping_string", "Payload String")
-- DP related fields
f.DPRef = ProtoField.uint16("sedbgmux.dpref", "Data Provider Reference", base.HEX_DEC)
f.DPNameLength = ProtoField.uint8("sedbgmux.dpname_length", "Data Provider Name Length")
f.DPNameString = ProtoField.string("sedbgmux.dpname_string", "Data Provider Name String")
-- Conn related fields
f.ConnRef = ProtoField.uint16("sedbgmux.connref", "Connection Reference", base.HEX_DEC)
f.ConnData = ProtoField.bytes("sedbgmux.conn_data", "Connection Data")
f.DataBlockLimit = ProtoField.uint8("sedbgmux.data_block_limit", "Data Block Limit")
f.DataBlockCount = ProtoField.uint8("sedbgmux.data_block_count", "Data Block Count")

function dis_ident(buffer, pinfo, subtree)
    subtree:add(f.IdentMagic, buffer(0, 4))
    subtree:add(f.IdentLength, buffer(4, 1))
    subtree:add(f.IdentString, buffer(5))
end

function dis_ping_pong(buffer, pinfo, subtree)
    subtree:add(f.PingPongLength, buffer(0, 1))
    subtree:add(f.PingPongString, buffer(1))
end

function dis_dp_announce(buffer, pinfo, subtree)
    subtree:add_le(f.DPRef, buffer(0, 2))
    subtree:add(f.DPNameLength, buffer(2, 1))
    subtree:add(f.DPNameString, buffer(3))
end

function dis_conn_establish(buffer, pinfo, subtree)
    subtree:add_le(f.DPRef, buffer(0, 2))
end

function dis_conn_established(buffer, pinfo, subtree)
    subtree:add_le(f.DPRef, buffer(0, 2))
    subtree:add_le(f.ConnRef, buffer(2, 2))
    subtree:add_le(f.DataBlockLimit, buffer(4, 2))
end

function dis_conn_terminate(buffer, pinfo, subtree)
    subtree:add_le(f.ConnRef, buffer(0, 2))
end

function dis_conn_terminated(buffer, pinfo, subtree)
    subtree:add_le(f.DPRef, buffer(0, 2))
    subtree:add_le(f.ConnRef, buffer(2, 2))
end

function dis_conn_data(buffer, pinfo, subtree)
    subtree:add_le(f.ConnRef, buffer(0, 2))
    subtree:add(f.ConnData, buffer(2))
end

function dis_flow_control(buffer, pinfo, subtree)
    subtree:add_le(f.ConnRef, buffer(0, 2))
    subtree:add(f.DataBlockCount, buffer(2, 1))
end

local MsgType_func = {
    [0x66] = dis_ident,
    [0x67] = dis_ping_pong,
    [0x68] = dis_ping_pong,
    [0x69] = dis_dp_announce,
    [0x6b] = dis_conn_establish,
    [0x6c] = dis_conn_established,
    [0x6d] = dis_conn_terminate,
    [0x6e] = dis_conn_terminated,
    [0x6f] = dis_conn_data,
    [0x70] = dis_flow_control,
}

-- Last dissected frame number
local last_fn = nil

function sedbgmux_proto.dissector(buffer, pinfo, tree)
    -- Ensure the minimum length (Marker + Length)
    if buffer:len() < (2 + 2) then
        return
    end
    -- Check the frame marker
    if buffer(0, 2):uint() ~= 0x4242 then
        return
    end

    pinfo.cols.protocol = sedbgmux_proto.name

    local subtree = tree:add(sedbgmux_proto, buffer(), "SEDbgMux Frame")
    local ti -- Tree Item

    -- Frame Marker
    subtree:add(f.Marker, buffer(0, 2))

    -- Frame Length
    ti = subtree:add_le(f.Length, buffer(2, 2))
    local length = buffer(2, 2):le_uint()
    local msg_len = length - (2 + 2 + 1) -- Length + {Tx,Rx}Count + MsgType
    if msg_len < 0 then
        ti:add_expert_info(PI_MALFORMED, PI_ERROR, "Malformed length (too low)")
        return
    end

    subtree:set_len(length + 2 + 2) -- Marker + Length

    -- Tx/Rx Count
    subtree:add(f.TxCount, buffer(4, 1))
    subtree:add(f.RxCount, buffer(5, 1))

    -- Message type
    subtree:add(f.MsgType, buffer(6, 1))
    local msg_type = buffer(6, 1):uint()

    -- Update the subtree and info column text
    local info = string.format("(Ns=%03u, Nr=%03u) %s",
                               buffer(4, 1):uint(),
                               buffer(5, 1):uint(),
                               MsgType_vs[msg_type])
    subtree:append_text(": " .. info)
    if last_fn == pinfo.number then
        pinfo.cols.info:append(", " .. MsgType_vs[msg_type])
    else
        pinfo.cols.info:set(info)
    end
    last_fn = pinfo.number

    -- (Optional) Message Data
    if msg_len > 0 then
        local msg_data = buffer(7, msg_len)
        ti = subtree:add(f.MsgData, msg_data)
        if MsgType_func[msg_type] then
            MsgType_func[msg_type](msg_data, pinfo, ti)
        end
    end

    -- Frame Check Sequence
    ti = subtree:add_le(f.FCS, buffer(7 + msg_len, 2))
    -- (Optional) FCS validation
    if sedbgmux_proto.prefs.check_fcs then
        local fcs = buffer(7 + msg_len, 2):le_uint()
        local fcs_calc = calc_fcs(buffer(0, length + 2))

        if fcs == fcs_calc then
            ti:append_text(" [valid]")
        else
            ti:append_text(" [invalid]")
            ti:add_expert_info(PI_CHECKSUM, PI_ERROR, "FCS mismatch, expected " .. fcs_calc)
        end
    else
        ti:append_text(" [validation disabled]")
    end

    -- Dissect pending DebugMux frames in the given buffer
    if buffer:len() > (length + 2 + 2) then
        sedbgmux_proto.dissector(buffer(length + 2 + 2):tvb("test"), pinfo, tree)
    end
end

-- vim:set ts=4 sw=4 et:
