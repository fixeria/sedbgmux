sedbgmux - DebugMux client for [Sony] Ericsson phones and modems
================================================================

```
  _____ ______ _____  _           __  __
 / ____|  ____|  __ \| |         |  \/  |
| (___ | |__  | |  | | |__   __ _| \  / |_   ___  __
 \___ \|  __| | |  | | '_ \ / _` | |\/| | | | \ \/ /
 ____) | |____| |__| | |_) | (_| | |  | | |_| |>  <
|_____/|______|_____/|_.__/ \__, |_|  |_|\__,_/_/\_\
                             __/ |
                            |___/
```

This repository contains an implementation of DebugMux client in Python, which
can be used to communicate with a DebugMux server.  The goal of this project
is purely educational;  it's mostly driven by my curiosity to understand how
TEMS Investigation software (which is also using DebugMux) does its job.

All the knowledge about DebugMux obtained by analysing the protocol traces
and looking at format strings found in the firmware images.  Not all parts
of the protocol are understood yet, but the current implementation is mature
enough to be used and can be considered 90% complete.

### Usage

The following uilities are currently available:

* `sedbgmux-shell.py` - interactive shell.  Can be used to activate DebugMux
  mode, identify the target device, and establish connections with DPs.
* `sedbgmux-dump.py` - dump management utility.  Can be used to analyze
  DebugMux frame captures and convert between various dump formats.

Below is an example of using `sedbgmux-shell.py`:

```
$ ./sedbgmux-shell.py -p /dev/ttyACM0
Welcome to DebugMux client for [Sony] Ericsson phones and modems!
DebugMux ('/dev/ttyACM0')> connect
DebugMux ('/dev/ttyACM0')> enquiry
[INFO] client.py:185 Identified target: 'Sony Ericsson C510', IMEI=354008032409208
[INFO] client.py:191 Data Provider available (DPRef=0xe494): 'OSEGW! 100 1'
[INFO] client.py:191 Data Provider available (DPRef=0xe495): 'Tvp'
[INFO] client.py:191 Data Provider available (DPRef=0xe496): 'ACC - Print Server Channel'
[INFO] client.py:191 Data Provider available (DPRef=0xe497): 'APP - Print Server Channel'
[INFO] client.py:191 Data Provider available (DPRef=0xe498): 'SDKSERVER'

DebugMux ('/dev/ttyACM0')> show target-info
Name: Sony Ericsson C510
IMEI: 354008032409208

DebugMux ('/dev/ttyACM0')> establish 0xe496 terminal
[INFO] base.py:78 Establishing connection with DPRef=0xe496
[INFO] client.py:199 Rx ConnEstablished: ConnRef=0x3d00, DPRef=0xe496
[INFO] base.py:94 Connection established: DPRef=0xe496, ConnRef=0x3d00, DataBlockLimit=256
... Hit Ctrl + C to escape and terminate connection
[INFO] base.py:87 Terminating connection ConnRef=0x3d00 with DPRef=0xe496
[INFO] client.py:220 Rx ConnTerminated: ConnRef=0x3d00, DPRef=0xe496
[INFO] base.py:104 Connection terminated: DPRef=0xe496, ConnRef=0x3d00
[INFO] client.py:191 Data Provider available (DPRef=0xe499): 'ACC - Print Server Channel'
```

The following connection handlers are currently available:

 - `terminal` - interactive terminal (using stdin/stdout),
 - `file-logger` - log received data to the specified file,
 - `udp-proxy` - send/receive data via a UDP socket,
 - `hexdump` - print hexdump for received data blocks,
 - `walker` - recursively list available entries in `Interactive Debug` DPs.

DebugMux Basics
---------------

DebugMux is a vendor specific interface/protocol exposed by Sony Ericsson
phones (A1 DB2xxx, A2 DB3xxxx) and Ericsson modems.  Similar to QMI (Qualcomm
MSM Interface), it allows debugging various firmware subsystems at run-time.

```
          Host side             :     Target side (phone/modem)
                                :
+---------------------------+   :   +---------------------------+
| Client Applications (CAs) |   :   |   Data Providers (DPs)    |
+---------------------------+   :   +---------------------------+
|      DebugMux Client      |   :   |      DebugMux Server      |
+---------------------------+   :   +---------------------------+
|      Transport Layer      |   :   |      Transport Layer      |
|---------------------------|   :   |---------------------------|
|   USB  |   IrDA   |   BT  |<----->|   USB  |   IrDA   |   BT  |
+---------------------------+   :   +---------------------------+
```

The above diagram illustrates the client and server components.  Various
subsystems of the phone/modem firmware can register as `Data providers`.
The host side `Client Applications` can establish one-to-one connections
with the registered `Data Providers`.  DebugMux basically provides means
for `CAs` and `DPs` to communicate with each other, multiplexing their
connections over a certain transport (such as USB, IrDA, or Bluetooth).

DebugMux communication is usually established from the client by sending
`AT*EDEBUGMUX` command to the AT interpreter.  This command switches the
modem connection into binary mode and transfers control over the channel
to the `DebugMux Server`.  It's yet unknown how to switch the channel
back to the command interpreter mode (neither sending `+++` nor setting
the modem status likes works), but forcing disconnection of the
`Transport Layer` (e.g. by unplugging the USB cable) works.

Data Providers
--------------

Below is the list of known DPs:

| **Name** | **Mode** | **Description** |
|----------|----------|-----------------|
| `Tvp` | Binary | Test and Verification Protocol |
| `Print Server Channel` | ASCII (Rx only) | APP/ACC CPU debug logging |
| `ACC - Print Server Channel` | ASCII (Rx only) | ACC (access) CPU debug logging |
| `APP - Print Server Channel` | ASCII (Rx only) | APP (application) CPU debug logging |
| `Interactive Debug` | ASCII (Rx/Rx) | APP/ACC CPU interactive debug |
| `ACC - Interactive Debug` | ASCII (Rx/Tx) | ACC (access) CPU interactive debug |
| `APP - Interactive Debug` | ASCII (Rx/Tx) | APP (application) CPU interactive debug |
| `UI Debug - Print Server Channel` | ASCII (Rx only) | UI (User Interface) debug logging |
| `AT channel (NUM)` | ASCII (Rx/Tx) | AT command interpreter |

Below is the list of DPs with unknown purpose:

| **Name** | **Comments** |
|----------|-----------------|
| `SDKSERVER` | seen mostly on all A2 phones |
| `OSEGW! 100 1` | seen on C510 (R1HA035), C905 (R1FA035), F5521gw (R2A07), G705 (R1FA035) |
| `Host Interface Test File Transfer` | OBEX? seen on W595 (R3EA037), K850 (R1FA035) |
| `Turning Torso` | A skyscraper in Sweden? seen on T700 (R3CA017), K850 (R1FA035) |
| `UI Trace Tool - Data` | seen on U100 (R7BA084, R7AA076), W20 (R7GE075) |

### `Interactive Debug` DPs

Sony Ericsson phones flashed with development firmware builds and some Ericsson
modems running the "normal" consumer firmware expose `Interactive Debug` DPs.
These DPs offer a command prompt and give access to a virtual filesystem
with lots of hidden parameters.

```
DebugMux ('/dev/ttyACM0')> establish 0x9b42 terminal
[INFO] base.py:78 Establishing connection with DPRef=0x9b42
[INFO] client.py:199 Rx ConnEstablished: ConnRef=0x3200, DPRef=0x9b42
[INFO] base.py:94 Connection established: DPRef=0x9b42, ConnRef=0x3200, DataBlockLimit=300

Welcome to Interactive Debug

[root]
```

The following UNIX-like commands are available:

 - `.` - repeat the last command,
 - `ls` / `dir` - directory listing,
 - `cd` - change directory (CWD displayed in prompt),
 - `r` / `w` - read / write a variable.

All input is case sensitive.  Tab-completion is available, but not working
properly with the existing connection handlers (should I add telnet for that?).

You can find directory listings for some phones/modems in `samples` folder:

| **Device** | **Firmware (AT+CGMR)** | **File** |
|------------|------------------------|----------|
| Ericsson F3607gw | R1K06 | `samples/f3607gw_acc_interactive_debug.list` |
| Ericsson F3607gw | R1K06 | `samples/f3607gw_app_interactive_debug.list` |
| Ericsson F5521gw | R2A07 | `samples/f5521gw_acc_interactive_debug.list` |
| Ericsson F5521gw | R2A07 | `samples/f5521gw_app_interactive_debug.list` |
| Ericsson MD400 | R4A009 | `samples/md400_acc_interactive_debug.list` |
| Ericsson MD400 | R4A009 | `samples/md400_app_interactive_debug.list` |
| Ericsson MD400g | R6A001 | `samples/md400g_acc_interactive_debug.list` |
| Ericsson MD400g | R6A001 | `samples/md400g_app_interactive_debug.list` |
| Sony Ericsson T700 | R3CA017 | `samples/t700_dev_acc_interactive_debug.list` |
| Sony Ericsson T700 | R3CA017 | `samples/t700_dev_app_interactive_debug.list` |
| Sony Ericsson K610 | R1BA022 | `samples/k610_dev_interactive_debug.list` |
| Sony Ericsson M600 | R9F011 | `samples/M600_R9F011_interactive_debug.list` |
| Sony Ericsson EC400 | R3A002 | `samples/EC400_R3A002_acc_interactive_debug.list` |
| Sony Ericsson EC400 | R3A002 | `samples/EC400_R3A002_app_interactive_debug.list` |
| Sony Ericsson MD300 | R4A007 | `samples/md300_R4A007_acc_interactive_debug.list` |
| Sony Ericsson MD300 | R4A007 | `samples/md300_R4A007_app_interactive_debug.list` |

Contributions are welcome!  Use the 'walker' handler to generate such listings.

DebugMux Protocol
-----------------

The protocol is relatively simple and shares some similarities with HDLC.
DebugMux peers communicate using frames, which have the following structure:

```
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|       Marker (\x42\x42)       |          Length (LE)          |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|    TxCount    |    RxCount    |    MsgType    |               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+               +
|                            MsgData                            |
+                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               |              FCS              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

The following message types are known:

| **Value**    | **Direction** | **Name** | **Description** |
|--------------|---------------|----------|-----------------|
| `0x65 ('e')` | -> | Enquiry | Enquiry the target info and available DPs |
| `0x66 ('f')` | <- | Ident | Target info (model, IMEI) |
| `0x67 ('g')` | -> | Ping | Used for connection testing |
| `0x68 ('h')` | <- | Pong | Used for connection testing |
| `0x69 ('i')` | <- | DPAnnounce | `Data Provider` availability announce |
| `0x6b ('k')` | -> | ConnEstablish | Connection establishment command |
| `0x6c ('l')` | <- | ConnEstablished | Connection establishment result |
| `0x6d ('m')` | -> | ConnTerminate | Connection termination command |
| `0x6e ('n')` | <- | ConnTerminated | Connection termination result |
| `0x6f ('o')` | <-> | ConnData | Connection data |
| `0x70 ('p')` | <- | FlowControl | Connection flow control |
| `0x71 ('q')` | <-> | Ack | Acknowledge |

Other DebugMux Software
-----------------------

The following software is known to "speak" DebugMux protocol:

* "DbgMuxLogger" by Ericsson Mobile Platforms AB
* "DbgMuxServer" by Ericsson Mobile Platforms AB
* "EMBLA Logger" by Ericsson AB
* "TEMS Investigation" by Ericsson/Ascom/Infovista
* "TSCom/TSProbe/TSTool" by Ericsson Mobile Platforms AB (?)

Those are all proprietary, black-box binaries for MS Windows.

Further reading/watching
------------------------

* OsmoDevCon 2024 talk "DebugMux: getting baseband logs from old Sony Ericsson phones"
  * video: https://media.ccc.de/v/osmodevcon2024-185-debugmux-getting-baseband-logs-from-old-sony-ericsson-phones
  * slides: https://people.osmocom.org/fixeria/talks/osmodevcon/sedbgmux/sedbgmux.html ([source code](doc/sedbgmux.adoc))
