# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2022  Vadim Yanitskiy <axilirator@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import abc


class DbgMuxIOError(Exception):
    ''' I/O error during read/write operation '''

class DumpIOError(Exception):
    ''' Dump I/O error '''

class DumpIOFcsError(DumpIOError):
    ''' Dump I/O Frame Check Sequence error '''

class DumpIOEndOfFile(DumpIOError):
    ''' Dump I/O EOF error '''


class DbgMuxIO(abc.ABC):
    ''' Abstract I/O layer for DebugMux '''

    @abc.abstractmethod
    def connect(self) -> None:
        ''' Establish connection to the target and enter DebugMux mode '''

    @abc.abstractmethod
    def disconnect(self) -> None:
        ''' Escape DebugMux mode and terminate connection with the target '''

    @abc.abstractmethod
    def read(self, length: int = 0) -> bytes:
        ''' Read the given number of bytes '''

    @abc.abstractmethod
    def write(self, data: bytes) -> int:
        ''' Write the given data bytes '''


class DumpIO(abc.ABC):
    ''' Abstract dump I/O interface '''

    ARGS = {}

    @abc.abstractmethod
    def read(self) -> dict:
        ''' Read a single record from dump '''

    @abc.abstractmethod
    def write(self, record: dict) -> None:
        ''' Store a single record to dump '''
