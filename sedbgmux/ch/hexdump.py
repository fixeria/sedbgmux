# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2024  Vadim Yanitskiy <fixeria@osmocom.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging

from construct.lib import hexdump

from . import DbgMuxConnHandler

# local logger for this module
log = logging.getLogger(__name__)


class DbgMuxConnHexDump(DbgMuxConnHandler):
    ''' Dummy connection handler printing received data blocks '''

    def __init__(self, linesize: int = 16, level: str = 'info'):
        super().__init__()
        self.linesize = linesize
        self.level = logging.getLevelName(level)

    def _conn_data(self, data: bytes) -> None:
        ''' Called on receipt of a data block '''
        log.log(self.level, '(DPRef=0x%04x, ConnRef=0x%04x) Rx ConnData: %s',
                self.DPRef, self.ConnRef, hexdump(data, self.linesize))
