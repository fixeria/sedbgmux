# This file is a part of sedbgmux, an open source DebugMux client.
# Copyright (c) 2022  Vadim Yanitskiy <axilirator@gmail.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
import threading
import queue

from typing import Any, Optional
from construct import Const, Container, Int16ul

from .io import DbgMuxIO
from .io import DumpIO
from . import DbgMuxFrame

# local logger for this module
log = logging.getLogger(__name__)


class DbgMuxPeer:
    def __init__(self, io: DbgMuxIO):
        self.dump: Optional[DumpIO] = None
        self.tx_count: int = 0
        self.rx_count: int = 0
        self.io = io

        # Threads handling Rx/Tx frames
        self._rx_thread = threading.Thread(target=self._rx_worker,
                                           name='DbgMuxPeer-Rx',
                                           daemon=True)
        self._tx_thread = threading.Thread(target=self._tx_worker,
                                           name='DbgMuxPeer-Tx',
                                           daemon=True)

        self._shutdown = threading.Event()

        # Internal queues for Rx/Tx frames
        self._rx_queue = queue.Queue()
        self._tx_queue = queue.Queue()

    def enable_dump(self, dump: DumpIO):
        self.dump = dump

    def start(self) -> None:
        self._shutdown.clear()
        self._rx_thread.start()
        self._tx_thread.start()

    def stop(self) -> None:
        # Set the shutdown event
        self._shutdown.set()
        # Wait for both threads to terminate
        self._tx_thread.join()
        self._rx_thread.join()

    def _rx_worker(self) -> None:
        while not self._shutdown.is_set():
            frame = self._recv() # blocking until timeout
            if frame is not None:
                self._rx_queue.put(frame)
        log.debug('Thread \'%s\' is shutting down', threading.current_thread().name)

    def _tx_worker(self) -> None:
        while not self._shutdown.is_set():
            try:
                (msg_type, msg) = self._tx_queue.get(block=True, timeout=0.5)
                self._send(msg_type, msg)
                self._tx_queue.task_done()
            except queue.Empty:
                pass
        log.debug('Thread \'%s\' is shutting down', threading.current_thread().name)

    def send(self, msg_type: DbgMuxFrame.MsgType, msg: Any = b'') -> None:
        ''' Send a single message (non-blocking call) '''
        self._tx_queue.put((msg_type, msg))

    def _send(self, msg_type: DbgMuxFrame.MsgType, msg: Any = b'') -> None:
        # Encode the inner message first
        msg_data = DbgMuxFrame.Msg.build(msg, MsgType=msg_type)

        c = Container({
            'TxCount': (self.tx_count + 1) % 256,
            'RxCount': self.rx_count % 256,
            'MsgType': msg_type,
            'MsgData': msg_data,
            'FCS': 0  # Calculated below
        })

        # ACK is a bit special
        if msg_type == DbgMuxFrame.MsgType.Ack:
            c['TxCount'] = 0xf1

        # There is a Checksum construct, but it requires all checksummed fields
        # to be wrapped into an additional RawCopy construct.  This is ugly and
        # inconvinient from the API point of view, so we calculate the FCS manually:
        frame = DbgMuxFrame.Frame.build(c)[:-2]  # strip b'\x00\x00'
        c['FCS'] = DbgMuxFrame.fcs_func(frame)
        frame += Int16ul.build(c['FCS'])

        log.debug('Tx frame (Ns=%03u, Nr=%03u, fcs=0x%04x) %s %s',
                  c['TxCount'], c['RxCount'], c['FCS'],
                  c['MsgType'], c['MsgData'].hex())

        self.io.write(frame)
        if self.dump is not None:
            record = dict(dir='Tx', data=frame)
            self.dump.write(record)

        # ACK is not getting accounted
        if msg_type != DbgMuxFrame.MsgType.Ack:
            self.tx_count += 1

    def recv(self, timeout: Optional[float] = None) -> Container:
        ''' Receive a single message (blocking call) '''
        return self._rx_queue.get(block=True, timeout=timeout)

    def _recv(self) -> Optional[Container]:
        frame: bytes = b''
        frame += self.io.read(2)  # Magic
        if frame == b'':
            return None
        Const(b'\x42\x42').parse(frame[:2])
        frame += self.io.read(2)  # Length
        length: int = Int16ul.parse(frame[2:])
        frame += self.io.read(length)  # Rest

        if self.dump is not None:
            record = dict(dir='Rx', data=frame)
            self.dump.write(record)

        c = DbgMuxFrame.Frame.parse(frame)

        log.debug('Rx frame (Ns=%03u, Nr=%03u, fcs=0x%04x) %s %s',
                  c['TxCount'], c['RxCount'], c['FCS'],
                  c['MsgType'], c['MsgData'].hex())

        # Re-calculate and check the FCS
        fcs = DbgMuxFrame.fcs_func(frame[:-2])
        if fcs != c['FCS']:
            log.error('Rx frame (Ns=%03u, Nr=%03u) with bad FCS: '
                      'indicated 0x%04x != calculated 0x%04x',
                      c['TxCount'], c['RxCount'], c['FCS'], fcs)
            # TODO: NACK this frame?

        # Parse the inner message
        c['Msg'] = DbgMuxFrame.Msg.parse(c['MsgData'], MsgType=c['MsgType'])

        # ACK is not getting accounted
        if c['MsgType'] != DbgMuxFrame.MsgType.Ack:
            self.rx_count += 1

        return c
